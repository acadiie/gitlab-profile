# ACADIIE

**Automated Comprehension And Analysis of Documents for Individual Information Extraction**

- Beginning of the project: 10/2023.
- End of the project: 09/2024
- Collaboration under Framework ‘France Relance’, between Laboratoire L3i and TEKLIA.

## Papers

- `[1]` Guillaume Bernard, Casey Wall, Mickaël Coustaty, Antoine Doucet. Un corpus de tables de recensement historiques : publication, analyse et extraction de lignes. Symposium International Francophone sur l'Ecrit et le Document (SIFED 2023), Jun 2023, Paris, France. ⟨https://hal.science/hal-04113017)
- `[2]` Guillaume Bernard, Casey Wall, Mélodie Boillet, Mickaël Coustaty, Christopher Kermorvant, et al.. Text Line Detection in Historical Index Tables: Evaluations on a New French PArish REcord Survey Dataset (PARES). The 25th International Conference on Asia-Pacific Digital Libraries, Hao-Ren KE (National Taiwan Normal University, Taiwan), Dec 2023, Taipei, Taiwan. ⟨https://hal.science/hal-04207205⟩

The sources of the papers are published on ULR’s GitLab, under `acadiie` namespace: [papers_and_publications](https://gitlab.univ-lr.fr/acadiie/papers_and_publications).

## Source codes

Source codes are hosted on ULR’s GitLab, under [`acadiie`](https://gitlab.univ-lr.fr/acadiie) namespace.

If the source codes of a paper have to be published, they are shared on [Software Heritage](https://www.softwareheritage.org/?lang=fr).

In november 2023, three repositories are archived on Software Heritage:
- `doc-ufcn-test`: [swh:1:dir:7ca17e4a36ff25cf4d68513a2af99074a3af4f3f](https://archive.softwareheritage.org/swh:1:dir:7ca17e4a36ff25cf4d68513a2af99074a3af4f3f)
- `doc-ufcn-utilities`: [swh:1:dir:ca5daf53c31def70e46c9aa8f887abe60cdd1d27](https://archive.softwareheritage.org/swh:1:dir:ca5daf53c31def70e46c9aa8f887abe60cdd1d27)
- `mask-rcnn`: [swh:1:dir:981ec0052f93e37505eba3d47e085a255483441f](https://archive.softwareheritage.org/swh:1:dir:981ec0052f93e37505eba3d47e085a255483441f)

These codes are mentionned in the paper `[2]`.

## Datasets

- Dataset and experiments are stored locally in the L3i, on `wnas.univ-lr.fr`, under `2023-ACADIIE`. It contains a copy of the datasets published on Zenodo, in addition to the original images (`acadiie_ined_images_vic_sur_seille_echevronne.tar`, 76Go) and the original transcriptions (`acadiie_ined_dataset_transcriptions_tables_echevronne_vic_sur_seille.ods`, 512Ko).

- Datasets are published on Zenodo:
  - PARESv1: https://zenodo.org/records/8337504
  - PARESv2: https://zenodo.org/records/10034553
     
## Experiments

- Experiments are published on Zenodo:
  - Experiments of 'Text Line Detection in Historical Index Tables: Evaluations on a New French PArish REcord Survey Dataset (PARESv1)': https://zenodo.org/records/8334664
    
 